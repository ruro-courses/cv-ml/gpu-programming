import numpy as np

from ..common.silence import ti
from ..common.timing import timed_fun

np_pad = timed_fun(name="numpy.pad")(np.pad)
ti_copy_from = timed_fun(ti.VectorNdarray.copy_from)
ti_fill = timed_fun(ti.VectorNdarray.fill)
ti_from_numpy = timed_fun(ti.VectorNdarray.from_numpy)
ti_to_numpy = timed_fun(ti.VectorNdarray.to_numpy)

to_numpy_type = ti.lang.util.to_numpy_type
to_taichi_type = ti.lang.util.to_taichi_type

NUM_CHANNELS = 3
RGB = range(NUM_CHANNELS)

VALID_NP_DTYPES = {np.uint8, np.float32}
VALID_NP_DTYPES |= {np.dtype(dt) for dt in VALID_NP_DTYPES}
VALID_TI_DTYPES = {to_taichi_type(dt) for dt in VALID_NP_DTYPES}

__all__ = [
    "NUM_CHANNELS",
    "RGB",
    "VALID_NP_DTYPES",
    "VALID_TI_DTYPES",
    "np",
    "np_pad",
    "ti",
    "ti_copy_from",
    "ti_fill",
    "ti_from_numpy",
    "ti_to_numpy",
    "timed_fun",
    "to_numpy_type",
    "to_taichi_type",
]
