from ._base import BaseFilter
from ._common import np, ti, ti_fill, ti_from_numpy, timed_fun


@timed_fun
@ti.kernel
def convolve(
    img: ti.any_arr(),
    out: ti.any_arr(),
    ndx: ti.template(),
    off: ti.template(),
    ker: ti.any_arr(),
):
    for i in ti.grouped(ti.ndrange(*ndx)):
        s = ti.Vector([0.0, 0.0, 0.0])
        for j in ti.grouped(ker):
            s += img[i - j + off] * ker[j][0]
        out[i] = s


class ConvolutionFilter(BaseFilter):
    kernels = []

    def __init__(self, *args):
        self._kernel_mats = mats = []
        self._kernel_pads = pads = []

        for kernel in self.kernels:
            assert isinstance(kernel, np.ndarray), "kernel must be a ndarray"
            kernel = kernel.astype(np.float32)
            kn, km = kernel.shape
            on, om = kn // 2, km // 2
            assert kn % 2 and km % 2, "kernel shape must be odd"

            mat = ti.Vector.ndarray(n=1, dtype=self.dtype, shape=kernel.shape)
            ti_from_numpy(mat, kernel[..., np.newaxis])
            mats.append(mat)
            pads.append((on, om))

        self.pad = tuple(np.sum(pads, axis=0).tolist())
        super().__init__(*args)
        self.buf = self.make_array()

    def load(self, img_npy):
        super().load(img_npy)
        ti_fill(self.buf, 0)

    def apply(self):
        for kernel, padding in zip(self._kernel_mats, self._kernel_pads):
            convolve(self.img, self.buf, self.range(padding), padding, kernel)
            self.img, self.buf = self.buf, self.img

        super().apply()
