from ._base import BaseFilter
from ._common import np, ti, timed_fun


@timed_fun
@ti.kernel
def gray_world(
    img: ti.any_arr(),
):
    mean = ti.Vector([0.0, 0.0, 0.0])
    for i in ti.grouped(img):
        ti.atomic_add(mean, img[i])

    eps = ti.static(np.finfo(np.float32).eps.item())
    h, w = img.shape
    mean /= h * w
    mean += eps
    r, g, b = mean
    mean_gray = (r + g + b) / ti.static(mean.n)
    mult = mean_gray / mean

    for i in ti.grouped(img):
        img[i] *= mult


class Gray(BaseFilter):
    """Gray World"""

    def apply(self):
        gray_world(self.img)
        super().apply()
