from ._common import (
    NUM_CHANNELS,
    VALID_NP_DTYPES,
    VALID_TI_DTYPES,
    np,
    np_pad,
    ti,
    ti_copy_from,
    ti_from_numpy,
    ti_to_numpy,
    timed_fun,
    to_numpy_type,
    to_taichi_type,
)


@ti.func
def f2u_scalar(v: ti.f32) -> ti.u8:
    v = ti.round(v)
    v = ti.select(v < 0, 0, v)
    v = ti.select(v > 255, 255, v)
    return v


@timed_fun
@ti.kernel
def quantize_f2u(img: ti.any_arr(), out: ti.any_arr()):
    for i in ti.grouped(img):
        out[i] = f2u_scalar(img[i])


@timed_fun
@ti.kernel
def scramble(img: ti.any_arr(), dt: ti.template()):
    for i in ti.grouped(img):
        for e in ti.static(range(img[i].n)):
            if ti.static(dt == ti.u8):
                img[i][e] = ti.random(ti.u32) % 256
            else:
                img[i][e] = 255 * ti.random(dt)


@ti.data_oriented
class BaseFilter:
    pad = (0, 0)
    mode = "edge"
    dtype = ti.f32
    inp_dtype = None
    out_dtype = None

    def __init__(self, shape, dtype):
        self.inp_dtype = dtype if self.inp_dtype is None else self.inp_dtype
        assert self.inp_dtype in VALID_NP_DTYPES

        self.out_dtype = dtype if self.out_dtype is None else self.out_dtype
        assert self.out_dtype in VALID_NP_DTYPES

        self._shape = shape[:2]
        self.img = self.make_array()
        self.out = self.make_array(dtype=to_taichi_type(self.out_dtype))

    def shape(self, pad=(0, 0)):
        return (
            self._shape[0] + 2 * pad[0],
            self._shape[1] + 2 * pad[1],
        )

    def range(self, pad=(0, 0)):
        shape = self.shape(self.pad)
        return (
            (pad[0], shape[0] - pad[0]),
            (pad[1], shape[1] - pad[1]),
        )

    def make_array(self, n=None, dtype=None):
        n = NUM_CHANNELS if n is None else n
        dtype = self.dtype if dtype is None else dtype
        shape = self.shape(self.pad)
        return ti.Vector.ndarray(n=n, dtype=dtype, shape=shape)

    def check_npy(self, img_npy):
        shape_x, shape_y, channels = img_npy.shape
        assert (shape_x, shape_y) == self._shape
        assert channels == NUM_CHANNELS
        assert img_npy.dtype == self.inp_dtype

    def init_random(self):
        assert isinstance(self.img, ti.VectorNdarray)
        assert self.img.dtype in VALID_TI_DTYPES
        scramble(self.img, self.img.dtype)

    def load(self, img_npy):
        self.check_npy(img_npy)
        img_npy = np_pad(
            img_npy,
            (
                (self.pad[0], self.pad[0]),
                (self.pad[1], self.pad[1]),
                (0, 0),
            ),
            mode=self.mode,
        )
        ti_from_numpy(self.img, img_npy)

    def store(self):
        img_npy = ti_to_numpy(self.out)
        center = [slice(*n) for n in self.range(self.pad)]
        return img_npy[tuple(center)]

    def apply(self):
        if self.out_dtype == np.uint8 and self.dtype != ti.u8:
            quantize_f2u(self.img, self.out)
        elif self.out_dtype == to_numpy_type(self.dtype):
            self.out, self.img = self.img, self.out
        else:
            ti_copy_from(self.out, self.img)
