from ..cli.common import bad_arg
from ._base import BaseFilter, f2u_scalar
from ._common import ti, timed_fun


@timed_fun
@ti.kernel
def get_pix_ybr(
    img: ti.any_arr(),
    ybr: ti.any_arr(),
):
    # Compute the brightness of each pixel
    weights = ti.Vector([0.2125, 0.7154, 0.0721])
    for i in ti.grouped(img):
        ybr_f = (img[i] * weights).sum()
        ybr[i][0] = f2u_scalar(ybr_f)


@timed_fun
@ti.kernel
def get_cdf_map(
    ybr: ti.any_arr(),
    cdf: ti.template(),
    bits: ti.template(),
):
    div = ti.static(2**bits)
    rem = ti.static(2 ** (8 - bits))
    bins = ti.Vector(div * [0])

    # Compute the histogram
    for i in ti.grouped(ybr):
        bin_idx = ybr[i][0] // rem

        # The following block is loosely equivalent to
        #     bins[bin_idx] += 1
        # but it doesn't use any indirection.
        # Note that the `ti.static` here indicates, that this
        # loop will be unrolled at compile time.
        for idx in ti.static(range(div)):
            ti.atomic_add(bins[idx], idx == bin_idx)

    for _ in range(1):
        # Convert histogram to cumulative distribution function
        cdf_i = 0
        for idx in ti.static(range(div)):
            cur = bins[idx] + cdf_i
            bins[idx] = cdf_i = cur

        # Create the mapping
        scale = 255.0 / cdf_i
        cdf[None][0] = 0.0
        for idx in ti.static(range(div)):
            cdf[None][1 + idx] = scale * bins[idx]


@timed_fun
@ti.kernel
def apply_lut(
    img: ti.any_arr(),
    ybr: ti.any_arr(),
    cdf: ti.template(),
    bits: ti.template(),
    gray: ti.template(),
):
    div = ti.static(2**bits)
    rem = ti.static(2 ** (8 - bits))

    # Apply the LUT map to image
    for i in ti.grouped(img):
        old_y = ybr[i][0]
        bin_idx = old_y // rem
        bin_off = (old_y % rem) / rem

        # The following block is loosely equivalent to
        #     new_ly = cdf[None][bin_idx]
        #     new_ry = cdf[None][1 + bin_idx]
        # but it doesn't use any indirection.
        # Note that the `ti.static` here indicates, that this
        # loop will be unrolled at compile time.
        new_ly = 0.0
        new_ry = 0.0
        for idx in ti.static(range(div)):
            match = idx == bin_idx
            new_ly = ti.select(match, cdf[None][idx], new_ly)
            new_ry = ti.select(match, cdf[None][1 + idx], new_ry)

        # Interpolate the new brightness value
        # linearly between next and previous cell
        new_y = bin_off * new_ry + (1 - bin_off) * new_ly

        if ti.static(gray):
            # Save the raw calibrated brightness
            new_i = new_y
            img[i].fill(new_i)
        else:
            # Adjust each channel (RGB)
            # according to the cdf delta
            new_i = f2u_scalar(new_y / old_y * img[i])
            img[i] = new_i


class HistEq(BaseFilter):
    """Histogram Equalization (Auto-contrast)"""

    dtype = ti.u8

    def __init__(
        self,
        *args,
        num_bits: int = 6,
        grayscale: bool = False,
    ):
        """
        num_bits: The number of bits used for the histogram. Must be in range [1, 8].
        grayscale: Instead of scaling the RGB values, save the calibrated brightness values as a grayscale image.
        """
        super().__init__(*args)
        if not 1 <= num_bits <= 8:
            bad_arg("The num-bits option must have a value in range [1; 8].")
        self.ybr = self.make_array(n=1)
        self.cdf = ti.Vector.field(n=1 + 2**num_bits, dtype=ti.f32, shape=())
        self.num_bits = num_bits
        self.grayscale = grayscale

    def apply(self):
        get_pix_ybr(self.img, self.ybr)
        ti.sync()
        get_cdf_map(self.ybr, self.cdf, self.num_bits)
        ti.sync()
        apply_lut(self.img, self.ybr, self.cdf, self.num_bits, self.grayscale)
        ti.sync()

        super().apply()
