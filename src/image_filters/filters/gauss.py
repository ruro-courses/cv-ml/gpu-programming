from ..common.silence import warn
from ._common import np
from ._conv import ConvolutionFilter

sqrt_2pi = np.sqrt(2 * np.pi)
min_radius = 1


def make_gaussian_1d(sigma, kernel_extent):
    if sigma < 0.25:
        warn(
            f"Requested sigma value {sigma} is probably too small to have any "
            "noticeable effect on the image. Try a larger value of sigma."
        )

    sigma_3 = np.ceil(kernel_extent * sigma).astype(int)
    radius = np.maximum(min_radius, sigma_3).item()
    x = np.linspace(-radius, radius, num=2 * radius + 1)
    norm_inner = -1 / (2 * sigma**2)
    norm_outer = 1 / (sigma * sqrt_2pi)
    gaussian = norm_outer * np.exp(norm_inner * x * x)
    gaussian /= gaussian.sum()
    return gaussian[np.newaxis]


class Gauss(ConvolutionFilter):
    """Gaussian Blur"""

    def __init__(
        self,
        *args,
        sigma: float = 10.0,
        use_single_kernel: bool = False,
        kernel_extent: float = 3.0,
    ):
        """
        sigma: The standard deviation of the gaussian.
        use_single_kernel: Use a single 2D kernel instead of 2 separable kernels.
        kernel_extent: Size of the kernel in terms of the sigma value. Defaults to the 3-sigma rule.
        """
        kernel = make_gaussian_1d(sigma, kernel_extent)
        if use_single_kernel:
            kernel = kernel * kernel.T
            self.kernels.append(kernel)
        else:
            self.kernels.append(kernel)
            self.kernels.append(kernel.T)
        super().__init__(*args)
