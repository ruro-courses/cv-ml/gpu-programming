from ..common.public import PublicExports


def _get_filt_attr(k):
    return "".join(s.capitalize() for s in k.split("_"))


filters = PublicExports(__name__, attr_func=_get_filt_attr)
__all__ = ["filters"]
