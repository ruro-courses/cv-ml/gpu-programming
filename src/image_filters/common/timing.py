import contextlib
import functools
import threading
import time

from ..common.silence import warn

timing_method = time.perf_counter_ns
timing_scale = 1_000_000_000
section_order = []
timers = {}


def sync():
    # No-Op unless monkey-patched
    pass


@contextlib.contextmanager
def timed(stats):
    sync()
    a = timing_method()

    yield

    sync()
    b = timing_method()

    with stats["lock"]:
        stats["times"].append((b - a) / timing_scale)
        stats["calls"] += 1


def empty_section():
    if section_order and section_order[-1] is not None:
        section_order.append(None)


def timers_setdefault(name, level, kind):
    if name in timers:
        if kind == "fun":
            warn(f"Timer for function {name} was created more than once!")
    else:
        section_order.append(name)
        timers[name] = {
            "lock": threading.Lock(),
            "kind": kind,
            "level": level,
            "times": [],
            "calls": 0,
        }
    return timers[name]


def timed_ctx(name, level=0):
    return timed(timers_setdefault(name, level, "ctx"))


def timed_fun(fun=None, *, name=None, level=None):
    if fun is None:
        return functools.partial(timed_fun, name=name, level=level)

    kernel = getattr(fun, "_is_wrapped_kernel", 0)

    if name is None:
        name = getattr(fun, "__qualname__", None)
    if name is None:
        name = getattr(fun, "__name__", repr(fun))
    if kernel:
        name += " [kernel]"

    if level is None:
        if kernel:
            level = 3
        else:
            level = 2

    stats = timers_setdefault(name, level, "fun")

    @functools.wraps(fun)
    def wrapped(*args, **kwargs):
        with timed(stats):
            return fun(*args, **kwargs)

    return wrapped


def fmt_time(t, u):
    for unit in ["s ", "ms", "μs"]:
        if unit.strip() == u:
            break
        t = t * 1_000
    else:
        unit = "ns"
        assert u == unit
    return f"{t:>.2f}{unit}"


def make_row(name, all_times, num_calls):
    total = sum(all_times)
    min_ = min(all_times)
    max_ = max(all_times)
    avg_ = total / num_calls

    total = fmt_time(total, "s")
    min_ = fmt_time(min_, "ms")
    max_ = fmt_time(max_, "ms")
    avg_ = fmt_time(avg_, "ms")
    num_calls = str(num_calls)

    return name, total, num_calls, min_, avg_, max_


def print_timings(level=0):
    plain_rows = [[]]
    cold_rows = [[]]
    hot_rows = [[]]

    for name in section_order:
        if name is None:
            if plain_rows[-1]:
                plain_rows.append([])
            if cold_rows[-1]:
                cold_rows.append([])
            if hot_rows[-1]:
                hot_rows.append([])
            continue

        timer = timers[name]
        name = f"`{name}`"

        if timer["level"] > level:
            continue

        all_times = timer["times"]
        num_calls = timer["calls"]

        if not num_calls:
            continue

        if num_calls > 1:
            cold, *all_times = all_times
            num_calls -= 1
            cold_rows[-1].append(make_row(name + " (cold)", [cold], 1))
            hot_rows[-1].append(make_row(name, all_times, num_calls))
        else:
            plain_rows[-1].append(make_row(name, all_times, num_calls))

    if not plain_rows[-1]:
        plain_rows.pop(-1)
    if not cold_rows[-1]:
        cold_rows.pop(-1)
    if not hot_rows[-1]:
        hot_rows.pop(-1)

    rows = plain_rows + cold_rows + hot_rows
    empty = [[("", "", "", "", "", "")]] * (2 * len(rows) - 1)
    empty[::2] = rows
    rows = sum(empty, [])

    headers = ["Title", "Total", "Calls", "Min", "Avg", "Max"]
    max_lens = [
        max(
            len(headers[idx]),
            max(len(row[idx]) for row in rows),
        )
        for idx in range(6)
    ]

    r_fmt = " | ".join(f"{{:>{max_lens[idx]}}}" for idx in range(6))
    c_fmt = " | ".join(f"{{:^{max_lens[idx]}}}" for idx in range(6))
    align = " | ".join("-" * (max_lens[idx] - 1) + ":" for idx in range(6))

    r_fmt = f"| {r_fmt} |"
    c_fmt = f"| {c_fmt} |"
    align = f"| {align} |"

    headers = c_fmt.format(*headers)
    print()
    print(headers)
    print(align)

    for row in rows:
        print(r_fmt.format(*row))

    print()
