# This file contains hacky workarounds that prevent taichi from spamming
# the console with useless warnings, headers and other information
import functools
import os
import sys
import tempfile

import click


class redirect_stream_fd:
    def __init__(self, stream):
        self.inside = False
        self.active = True

        self.target = tempfile.TemporaryFile("w+")
        self.stream = stream

        self.target_fd = self.target.fileno()
        self.stream_fd = self.stream.fileno()
        self.backup_fd = None

    def deactivate(self):
        # Exit early
        self.__exit__()

        # Print any currently captured data
        data = self.get()
        self.stream.write(data)
        self.stream.flush()

        # Disable further operations
        self.active = False

    def get(self):
        if self.active:
            self.target.seek(0)
            return self.target.read()
        else:
            return ""

    def __enter__(self):
        # Check state
        assert not self.inside, "redirect_stream_fd is not reentrant"
        self.inside = True
        if not self.active:
            return self.get

        # Dump previous output
        self.stream.flush()

        # Save file descriptor, that is currently used by the stream
        self.backup_fd = os.dup(self.stream_fd)

        # Replace the streams file descriptor with our target file descriptor
        os.dup2(self.target_fd, self.stream_fd)

        return self.get

    def __exit__(self, *_):
        # Check state
        if not self.inside:
            return
        self.inside = False
        if not self.active:
            return

        # Dump current output
        self.stream.flush()

        # Restore original file descriptor for stream
        os.dup2(self.backup_fd, self.stream_fd)

        # Clean up the backup file descriptor
        os.close(self.backup_fd)
        self.backup_fd = None


def silent_taichi():
    with redirect_stdout:
        import taichi

        return taichi


def silent(fun):
    @functools.wraps(fun)
    def _wraps(*args, **kwargs):
        with redirect_stdout:
            return fun(*args, **kwargs)

    return _wraps


redirect_stdout = redirect_stream_fd(sys.stdout)
ti = silent_taichi()
ti_init = silent(ti.init)
ti.Matrix.__init__ = functools.partialmethod(ti.Matrix.__init__, suppress_warning=True)


def warn(msg):
    click.secho("Warning! " + msg, err=True, fg="yellow")
