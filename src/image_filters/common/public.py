import collections.abc
import importlib
import modulefinder
import sys


class PublicExports(collections.abc.Mapping):
    def __init__(self, name, attr_func=lambda k: k):
        self.name = name
        self.attr_func = attr_func

        module = sys.modules[name]
        finder = modulefinder.ModuleFinder()
        submodules = finder.find_all_submodules(module)

        self.submodules = sorted(sub for sub in submodules if not sub.startswith("_"))

    def __len__(self):
        return len(self.submodules)

    def __iter__(self):
        return iter(self.submodules)

    def __getitem__(self, k):
        submodule = importlib.import_module("." + k, self.name)
        export = getattr(submodule, self.attr_func(k))
        return export
