from .common import make_option
from .kwargs.action import action_kwargs
from .kwargs.filter import filter_kwargs
from .kwargs.shared import shared_kwargs
from .segments.shared import SharedSegment

main = SharedSegment(
    action_kwargs=action_kwargs,
    filter_kwargs=filter_kwargs,
    **shared_kwargs,
)
__all__ = ["main"]
