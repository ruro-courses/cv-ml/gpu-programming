import click

from ..common.silence import redirect_stdout, ti, ti_init


def bad_opt(opt, msg):
    raise click.BadOptionUsage(opt, msg)


def bad_arg(msg):
    raise click.BadArgumentUsage(msg)


def internal_error(msg):
    raise RuntimeError(msg)


def bad_use(msg):
    raise click.UsageError(msg)


def ti_init_common(
    *,
    device,
    num_threads,
    debug,
    profile,
    dump_ir,
    disable_tls,
    log_level,
):
    if log_level:
        redirect_stdout.deactivate()

    print_ir = log_level > 2 and dump_ir

    device = getattr(ti, device)
    log_levels = ["error", "warn", "info", "debug", "trace"]
    if log_level < 0:
        log_level = log_levels[0]
    elif log_level >= len(log_levels):
        log_level = log_levels[-1]
    else:
        log_level = log_levels[log_level]

    init_args = dict(
        move_loop_invariant_outside_if=True,
        flatten_if=True,
        packed=True,
        offline_cache=True,
        make_thread_local=not disable_tls,
        device_memory_fraction=0.9,
        arch=device,
        debug=debug,
        check_out_of_bound=debug,
        print_ir=print_ir,
        print_kernel_nvptx=dump_ir,
        print_kernel_llvm_ir_optimized=dump_ir,
        kernel_profiler=profile,
        log_level=log_level,
    )

    if num_threads is not None:
        if device != ti.cpu:
            bad_opt(
                "num_threads",
                "Option num_threads is only valid when device=cpu.",
            )
        init_args["cpu_max_num_threads"] = num_threads

    return ti_init(**init_args)


def make_argument(arg, type_, **kwargs):
    arg = arg.replace("_", "-")

    return click.Argument(
        [arg],
        type=type_,
        **kwargs,
    )


def make_option(arg, type_, default, **kwargs):
    arg = arg.replace("_", "-")
    count = kwargs.pop("count", False)
    short = kwargs.pop("short", False)
    is_flag = type_ == bool
    show_default = kwargs.pop("show_default", str(default))
    help_ = kwargs.pop("help")
    short = [f"-{arg[0]}"] if short else []

    return click.Option(
        [f"--{arg}"] + short,
        type=type_,
        default=default,
        is_flag=is_flag,
        count=count,
        show_default=False,
        help=f"\b\n{help_}\n(default: {show_default})",
        **kwargs,
    )


common_kwargs = dict(
    no_args_is_help=True,
)
