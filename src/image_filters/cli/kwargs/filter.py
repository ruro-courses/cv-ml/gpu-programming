from ...filters import filters
from ..common import make_option


def make_filter_spec(filter_cls):
    return dict(
        filter_cls=filter_cls,
        options=make_filter_options(filter_cls),
        short_help=f"-  {filter_cls.__doc__}",
    )


def make_filter_options(filter_cls):
    options = []

    init = filter_cls.__init__

    docs = getattr(init, "__doc__", None)
    annots = getattr(init, "__annotations__", None)
    defaults = getattr(init, "__kwdefaults__", None)

    docs = "" if docs is None else docs
    annots = {} if annots is None else annots
    defaults = {} if defaults is None else defaults

    docs = docs.strip().splitlines()
    docs = [line.split(":") for line in docs]
    docs = {arg.strip(): doc.strip() for arg, doc in docs}

    assert set(docs) == set(annots) == set(defaults)

    for arg in docs:
        options.append(
            make_option(
                arg,
                type_=annots[arg],
                default=defaults[arg],
                help=docs[arg],
            )
        )

    return options


filter_kwargs = {
    filter_name: make_filter_spec(filter_cls)
    for filter_name, filter_cls in filters.items()
}
__all__ = ["filter_kwargs"]
