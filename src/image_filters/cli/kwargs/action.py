import pathlib

import click

from ..common import bad_arg, make_argument, make_option
from ..impl import action_impls


def shape_type(v):
    try:
        h = w = int(v)
    except ValueError:
        v = v.strip()
        for sep in ["x", ",", " "]:
            if sep in v:
                h, w = v.split(sep)
                break
        else:
            bad_arg("{v} is not a valid SHAPE.")
        h, w = int(h), int(w)
    return h, w


action_kwargs = dict(
    bench=dict(
        short_help="-  Measure and debug the performance characteristics of the filter.",
        metavar="SHAPE [RUNS]",
        options=[
            make_option(
                "verbose",
                type_=int,
                default=0,
                count=True,
                short=True,
                help=(
                    "Increase the amount of output. This also affects the output of\n"
                    "--profile, --dump-ir and --debug options. Repeated use of this\n"
                    "option further increases the verbosity up to a maximum of -vvvv."
                ),
            ),
            make_option(
                "skip-memory",
                type_=bool,
                default=False,
                help=(
                    "Skip the memory transfers between the host and device.\n"
                    "Initialize the image with random data straight on the device."
                ),
            ),
            make_option(
                "dtype",
                type_=click.Choice(["uint8", "float32"]),
                default="uint8",
                help="Choose, which element data type to use for the input image.",
            ),
            make_option(
                "profile",
                type_=bool,
                default=False,
                help="Collect and print profiling information.",
            ),
            make_option(
                "debug",
                type_=bool,
                default=False,
                help="Enable sanity checks and print debug information.",
            ),
            make_option(
                "dump-ir",
                type_=bool,
                default=False,
                help="Dump the intermediary representation of the compiled kernels.",
            ),
            make_option(
                "disable-tls",
                type_=bool,
                default=False,
                help="Disable thread-local storage and related optimizations.",
            ),
            make_argument(
                "shape",
                type_=shape_type,
            ),
            make_argument(
                "runs",
                type_=int,
                default=16,
            ),
        ],
    ),
    apply=dict(
        short_help="-  Load images, process them with some filter and then save the results.",
        metavar="INPUTS... OUTPUT",
        options=[
            make_option(
                "overwrite",
                type_=bool,
                default=False,
                help="Write the output image even if the OUTPUT file already exists.",
            ),
            make_argument(
                "inputs",
                type_=pathlib.Path,
                required=True,
                nargs=-1,
            ),
            make_argument(
                "output",
                type_=pathlib.Path,
            ),
        ],
    ),
)

assert set(action_kwargs) == set(action_impls)
action_kwargs = {
    action: dict(action_impl=action_impls[action], **action_kwargs[action])
    for action in action_impls
}
__all__ = ["action_kwargs"]
