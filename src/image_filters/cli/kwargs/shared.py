import shutil

import click

from ..common import make_option

shared_kwargs = dict(
    name="shared",
    context_settings=dict(
        max_content_width=shutil.get_terminal_size().columns,
        help_option_names=["-h", "--help"],
    ),
    help=(
        "\b\nNote: different filters and actions have slightly different options.\n"
        "You can see, which options are available for any combination like this:\n"
        "\b\n"
        "  > image_filters apply gauss --help\n"
        "  > image_filters bench gray  --help\n"
    ),
    options=[
        make_option(
            "device",
            type_=click.Choice(["cpu", "cuda", "vulkan"]),
            default="cuda",
            help="Taichi backend to use for filter computation.",
        ),
        make_option(
            "num_threads",
            type_=int,
            default=None,
            show_default="{cpu_count}",
            help="Number of CPU threads to use, only allowed for device=cpu.",
        ),
    ],
)
