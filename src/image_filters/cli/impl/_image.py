import numpy as np
from PIL import Image

from ...common.silence import warn
from ..common import bad_use, internal_error

try:
    import Imath
    import OpenEXR
except ImportError:
    OpenEXR = None
    Imath = None

# Load images of any size
Image.MAX_IMAGE_PIXELS = float("inf")


def warn_not_rgb(channels):
    warn(
        f"The input image has the following channels: {channels}.\n"
        f"Attempting to convert to RGB. Note, that any extra channels will be discarded."
    )


def load_exr(filename):
    warn("EXR image support isn't tested at this moment and may not work correctly.")

    if OpenEXR is None:
        bad_use("To load EXR images please install the optional OpenEXR dependency.")

    file = OpenEXR.InputFile(str(filename))
    header = file.header()

    window = header["dataWindow"]
    channel_shape = (
        window.max.y - window.min.y + 1,
        window.max.x - window.min.x + 1,
        1,
    )

    pix_fmt = Imath.PixelType(Imath.PixelType.FLOAT)
    pix_typ = np.float32

    if set(header["channels"]) != set("RGB"):
        warn_not_rgb("".join(header["channels"]))

    return np.concatenate(
        [
            np.frombuffer(file.channel(channel, pix_fmt), dtype=pix_typ,).reshape(
                channel_shape,
            )
            for channel in "RGB"
        ],
        axis=2,
    )


def load_pil(filename):
    img = Image.open(filename)
    if img.mode != "RGB":
        warn_not_rgb(img.mode)
    img = img.convert("RGB")
    img = np.asarray(img)
    return img


def load(filename):
    if filename.suffix.lower() == ".exr":
        return load_exr(filename)
    return load_pil(filename)


def save_exr(filename, img):
    warn("EXR image support isn't tested at this moment and may not work correctly.")

    if OpenEXR is None:
        bad_use("To save EXR images please install the optional OpenEXR dependency.")

    header = OpenEXR.Header(*img.shape[:2][::-1])
    file = OpenEXR.OutputFile(str(filename), header)
    return file.writePixels({c: img[..., i].tobytes() for i, c in enumerate("RGB")})


def save_pil(filename, img):
    return Image.fromarray(img).save(filename)


def save(filename, img):
    if img.ndim == 2:
        img = img[:, :, np.newaxis]
    elif img.ndim != 3:
        internal_error(f"Trying to save an invalid image with {img.ndim} dimensions.")

    if img.shape[-1] == 1:
        img = np.concatenate(3 * [img], axis=2)
    elif img.shape[-1] != 3:
        internal_error(
            f"Trying to save an invalid image with {img.shape[-1]} channels."
        )

    ext = filename.suffix.lower()
    if img.dtype == np.float32:
        if ext != ".exr":
            bad_use(
                "You must use the 'exr' file extension to save the output of this filter."
            )
    elif img.dtype != np.uint8:
        internal_error(f"Trying to save an invalid image with dtype {img.dtype}.")

    if ext == ".exr":
        return save_exr(filename, img)
    return save_pil(filename, img)
