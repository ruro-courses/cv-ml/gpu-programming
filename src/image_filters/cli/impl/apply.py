import contextlib

import click

from ..common import bad_arg, bad_use, ti_init_common
from ._image import load, save


def open_images(inputs, output, overwrite):
    if len(inputs) > 1 and not output.is_dir():
        bad_arg("Multiple inputs were provided, but output is not a directory.")

    for inp in inputs:
        out = output / inp.name if output.is_dir() else output

        if not inp.exists():
            bad_arg(f"Input path {str(inp)!r} doesn't exist.")

        if not inp.is_file():
            bad_arg(f"Input path {str(inp)!r} must be a file.")

        if out.exists() and not overwrite:
            bad_arg(
                f"Output path {str(out)!r} already exist.\n"
                "Use the --overwrite option if you want to overwrite the file."
            )

        try:
            img = load(inp)
        except Exception as ex:
            bad_use(
                f"Couldn't open input image {str(inp)!r}.\n"
                f"Reason: {str(ex).capitalize()}"
            )

        yield img, out


def apply_impl(
    filter_cls,
    inputs,
    output,
    device,
    num_threads,
    overwrite,
    **filter_kwargs,
):
    ti_init_common(
        device=device,
        num_threads=num_threads,
        debug=False,
        profile=False,
        dump_ir=False,
        disable_tls=False,
        log_level=0,
    )

    images = open_images(inputs, output, overwrite)
    if len(inputs) > 1:
        images = click.progressbar(images, length=len(inputs))
    else:
        images = contextlib.nullcontext(images)

    with images as images:
        for img, out in images:
            # Initialize filter
            filter_instance = filter_cls(img.shape, img.dtype, **filter_kwargs)

            # Load image numpy -> device
            filter_instance.load(img)

            # Run filter
            filter_instance.apply()

            # Store image device -> numpy
            img = filter_instance.store()

            # Save image to disk
            save(out, img)
