from ...common.public import PublicExports


def _get_impl_attr(k):
    return k + "_impl"


action_impls = PublicExports(__name__, attr_func=_get_impl_attr)
__all__ = ["action_impls"]
