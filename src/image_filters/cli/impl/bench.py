import numpy as np

from ...common import timing
from ...common.timing import empty_section, print_timings, timed_ctx
from ..common import ti, ti_init_common


def bench_impl(
    filter_cls,
    shape,
    runs,
    device,
    num_threads,
    verbose,
    skip_memory,
    dtype,
    profile,
    debug,
    dump_ir,
    disable_tls,
    **filter_kwargs,
):
    dtype = getattr(np, dtype)
    if dump_ir:
        verbose = max(1, verbose)

    def make_host_random():
        if dtype == np.uint8:
            return np.random.randint(0, 256, size=shape + (3,), dtype=dtype)
        return 128 + 64 * np.random.randn(*shape + (3,)).astype(dtype=dtype)

    empty_section()
    with timed_ctx("init_taichi"):
        ti_init_common(
            device=device,
            num_threads=num_threads,
            debug=debug,
            profile=profile,
            dump_ir=dump_ir,
            disable_tls=disable_tls,
            log_level=verbose,
        )

    # Enable synchronous timing
    timing.sync = ti.sync
    with timed_ctx("init_filter"):
        filter_instance = filter_cls(shape, dtype, **filter_kwargs)

    def trial():
        empty_section()
        if skip_memory:
            with timed_ctx("init_random"):
                filter_instance.init_random()
        else:
            with timed_ctx("host_to_device"):
                filter_instance.load(make_host_random())

        with timed_ctx("apply_filter"):
            filter_instance.apply()

        if not skip_memory:
            with timed_ctx("device_to_host"):
                _ = filter_instance.store()

    for _ in range(runs):
        trial()

    print_timings(level=verbose)

    if profile:
        ti.profiler.print_kernel_profiler_info()
        if verbose > 1:
            ti.profiler.print_scoped_profiler_info()
            ti.profiler.print_kernel_profiler_info("trace")
