import contextlib
import functools

from click import formatting

formatting.HelpFormatter.__init__ = functools.partialmethod(
    formatting.HelpFormatter.__init__,
    indent_increment=4,
)


def format_commands(commands, formatter, section):
    cmds = []
    for name, cmd in commands.items():
        # What is this, the tool lied about a command.  Ignore it
        if cmd is None:
            continue
        if getattr(cmd, "hidden", False):
            continue

        cmds.append((name, cmd))

    limit = formatter.width - 6 - max(len(cmd[0]) for cmd in cmds)

    rows = []
    for name, cmd in cmds:
        cmd = cmd if isinstance(cmd, str) else cmd.get_short_help_str(limit)
        rows.append((name, cmd))

    with formatter.section(section):
        formatter.write_dl(rows)


def write_dl(formatter, rows, widths, col_max=30, col_spacing=2):
    if len(widths) != 2:
        raise TypeError("Expected two columns for definition list")

    first_col = min(widths[0], col_max) + col_spacing

    for first, second in formatting.iter_rows(rows, len(widths)):
        formatter.write(f"{'':>{formatter.current_indent}}{first}")
        if not second:
            formatter.write("\n")
            continue
        if formatting.term_len(first) <= first_col - col_spacing:
            formatter.write(" " * (first_col - formatting.term_len(first)))
        else:
            formatter.write("\n")
            formatter.write(" " * (first_col + formatter.current_indent))

        text_width = max(formatter.width - first_col - 2, 10)
        wrapped_text = formatting.wrap_text(
            second,
            text_width,
            preserve_paragraphs=True,
        )
        lines = wrapped_text.splitlines()

        if lines:
            formatter.write(f"{lines[0]}\n")

            for line in lines[1:]:
                formatter.write(f"{'':>{first_col + formatter.current_indent}}{line}\n")
        else:
            formatter.write("\n")


def get_opts(options, ctx):
    opts = []
    for param in options:
        rv = param.get_help_record(ctx)
        if rv is not None:
            if opts:
                opts.append(("", ""))
            opts.append(rv)
    return opts


def get_level_description(level):
    if level.description is None:
        return None
    return f"{level.description.capitalize()} Options ({level.name})"


def format_nested_options(level, ctx, formatter, section):
    opts = [(get_level_description(level), level.options)]
    while level.parent is not None:
        level = level.parent
        opts.append((get_level_description(level), level.options))
    opts = opts[::-1]

    opts = [(s, get_opts(o, ctx)) for s, o in opts]
    all_opts = sum((o for _, o in opts), [])
    widths = formatting.measure_table(all_opts)

    with formatter.section(section):
        for s, o in opts:
            if not o:
                continue

            if s is None:
                s = contextlib.nullcontext()
                s_widths = widths
            else:
                s = formatter.section(s)
                w1, w2 = widths
                s_widths = (
                    w1 - formatter.indent_increment,
                    w1 + formatter.indent_increment,
                )

            with s:
                write_dl(formatter, o, s_widths)
