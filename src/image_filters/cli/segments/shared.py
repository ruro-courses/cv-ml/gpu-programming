import click

from ..common import common_kwargs
from ._formatting import format_commands, format_nested_options
from .action import ActionSegment


class SharedSegment(click.Group):
    parent = None
    description = None
    command_class = ActionSegment

    def __init__(
        self,
        *,
        # Basic
        options,
        # Kwargs
        action_kwargs,
        filter_kwargs,
        **shared_kwargs,
    ):
        self.options = options
        self.filters = {name: kw["short_help"] for name, kw in filter_kwargs.items()}

        super_kwargs = {}
        super_kwargs.update(common_kwargs)
        super_kwargs.update(shared_kwargs)

        super().__init__(
            options_metavar="\b",
            subcommand_metavar="ACTION FILTER [OPTIONS] ...",
            **super_kwargs,
        )

        for name, kwargs in action_kwargs.items():
            self.add_command(
                self.command_class(
                    name=name,
                    parent=self,
                    filter_kwargs=filter_kwargs,
                    **kwargs,
                )
            )

    def format_options(self, ctx, formatter):
        format_commands(self.commands, formatter, "Actions")
        format_commands(self.filters, formatter, "Filters")
        format_nested_options(self, ctx, formatter, "Options")
