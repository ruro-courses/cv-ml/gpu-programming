import click

from ..common import common_kwargs
from ._formatting import format_commands, format_nested_options
from .filter import FilterSegment


class ActionSegment(click.Group):
    description = "Action"
    command_class = FilterSegment

    def __init__(
        self,
        *,
        # Basic
        parent,
        options,
        # Special
        metavar,
        action_impl,
        # Kwargs
        filter_kwargs,
        **action_kwargs,
    ):
        self.options = options
        self.parent = parent
        self.metavar = metavar

        super_kwargs = {}
        super_kwargs.update(common_kwargs)
        super_kwargs.update(action_kwargs)
        super_kwargs["help"] = self.parent.help + "\n\n" + super_kwargs.get("help", "")

        super().__init__(
            options_metavar="\b",
            subcommand_metavar="FILTER [OPTIONS] " + metavar,
            **super_kwargs,
        )

        for name, kwargs in filter_kwargs.items():
            self.add_command(
                self.command_class(
                    name=name,
                    parent=self,
                    action_impl=action_impl,
                    **kwargs,
                )
            )

    def format_options(self, ctx, formatter):
        format_commands(self.commands, formatter, "Filters")
        format_nested_options(self, ctx, formatter, "Options")
