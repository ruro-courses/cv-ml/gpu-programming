import functools

import click

from ..common import common_kwargs
from ._formatting import format_nested_options


class FilterSegment(click.Command):
    description = "Filter"

    def __init__(
        self,
        *,
        # Basic
        parent,
        options,
        # Special
        action_impl,
        filter_cls,
        # Kwargs
        **filter_kwargs,
    ):
        self.options = options
        self.parent = parent

        super_kwargs = {}
        super_kwargs.update(common_kwargs)
        super_kwargs.update(filter_kwargs)
        super_kwargs["help"] = self.parent.help + "\n\n" + super_kwargs.get("help", "")

        params = self.options + self.parent.options + self.parent.parent.options
        callback = functools.partial(action_impl, filter_cls=filter_cls)
        super().__init__(
            params=params,
            callback=callback,
            **super_kwargs,
        )

    def format_options(self, ctx, formatter):
        format_nested_options(self, ctx, formatter, "Options")
