
|          Title          |  Total   | Calls |    Min     |    Avg     |    Max     |
| ----------------------: | -------: | ----: | ---------: | ---------: | ---------: |
|           `init_taichi` |   0.18s  |     1 |   182.44ms |   182.44ms |   182.44ms |
|           `init_filter` |   0.03s  |     1 |    27.65ms |    27.65ms |    27.65ms |
|                         |          |       |            |            |            |
| `host_to_device` (cold) |   0.06s  |     1 |    62.14ms |    62.14ms |    62.14ms |
|   `apply_filter` (cold) |  54.22s  |     1 | 54222.15ms | 54222.15ms | 54222.15ms |
| `device_to_host` (cold) |   0.05s  |     1 |    48.08ms |    48.08ms |    48.08ms |
|                         |          |       |            |            |            |
|        `host_to_device` |   0.08s  |     3 |    25.55ms |    26.99ms |    29.07ms |
|          `apply_filter` | 165.95s  |     3 | 54905.47ms | 55315.56ms | 55748.54ms |
|        `device_to_host` |   0.07s  |     3 |    21.31ms |    21.87ms |    22.26ms |

