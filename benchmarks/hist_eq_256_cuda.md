
|          Title          | Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -----: | ----: | --------: | --------: | --------: |
|           `init_taichi` | 0.82s  |     1 |  816.19ms |  816.19ms |  816.19ms |
|           `init_filter` | 0.08s  |     1 |   79.53ms |   79.53ms |   79.53ms |
|                         |        |       |           |           |           |
| `host_to_device` (cold) | 0.07s  |     1 |   74.05ms |   74.05ms |   74.05ms |
|   `apply_filter` (cold) | 1.72s  |     1 | 1715.13ms | 1715.13ms | 1715.13ms |
| `device_to_host` (cold) | 0.07s  |     1 |   73.63ms |   73.63ms |   73.63ms |
|                         |        |       |           |           |           |
|        `host_to_device` | 0.01s  |    15 |    0.59ms |    0.71ms |    1.56ms |
|          `apply_filter` | 0.01s  |    15 |    0.46ms |    0.48ms |    0.58ms |
|        `device_to_host` | 0.00s  |    15 |    0.22ms |    0.25ms |    0.37ms |

