
|          Title          |  Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | ------: | ----: | --------: | --------: | --------: |
|           `init_taichi` |  0.18s  |     1 |  184.91ms |  184.91ms |  184.91ms |
|           `init_filter` |  0.03s  |     1 |   29.93ms |   29.93ms |   29.93ms |
|                         |         |       |           |           |           |
| `host_to_device` (cold) |  0.42s  |     1 |  418.83ms |  418.83ms |  418.83ms |
|   `apply_filter` (cold) |  5.51s  |     1 | 5506.67ms | 5506.67ms | 5506.67ms |
| `device_to_host` (cold) |  0.34s  |     1 |  340.35ms |  340.35ms |  340.35ms |
|                         |         |       |           |           |           |
|        `host_to_device` |  1.13s  |     3 |  371.41ms |  378.22ms |  386.46ms |
|          `apply_filter` | 14.67s  |     3 | 4873.55ms | 4888.62ms | 4911.68ms |
|        `device_to_host` |  0.95s  |     3 |  314.04ms |  317.30ms |  322.61ms |

