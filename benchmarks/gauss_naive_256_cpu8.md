
|          Title          |  Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | ------: | ----: | --------: | --------: | --------: |
|           `init_taichi` |  0.18s  |     1 |  183.25ms |  183.25ms |  183.25ms |
|           `init_filter` |  0.03s  |     1 |   30.02ms |   30.02ms |   30.02ms |
|                         |         |       |           |           |           |
| `host_to_device` (cold) |  0.03s  |     1 |   30.87ms |   30.87ms |   30.87ms |
|   `apply_filter` (cold) |  3.46s  |     1 | 3458.77ms | 3458.77ms | 3458.77ms |
| `device_to_host` (cold) |  0.03s  |     1 |   27.47ms |   27.47ms |   27.47ms |
|                         |         |       |           |           |           |
|        `host_to_device` |  0.00s  |     3 |    1.26ms |    1.31ms |    1.37ms |
|          `apply_filter` | 10.14s  |     3 | 3374.19ms | 3379.10ms | 3382.99ms |
|        `device_to_host` |  0.01s  |     3 |    2.03ms |    2.05ms |    2.06ms |

