
|          Title          |  Total   | Calls |    Min     |    Avg     |    Max     |
| ----------------------: | -------: | ----: | ---------: | ---------: | ---------: |
|           `init_taichi` |   0.19s  |     1 |   194.24ms |   194.24ms |   194.24ms |
|           `init_filter` |   0.03s  |     1 |    28.80ms |    28.80ms |    28.80ms |
|                         |          |       |            |            |            |
| `host_to_device` (cold) |   0.05s  |     1 |    51.17ms |    51.17ms |    51.17ms |
|   `apply_filter` (cold) |  54.79s  |     1 | 54787.83ms | 54787.83ms | 54787.83ms |
| `device_to_host` (cold) |   0.05s  |     1 |    48.90ms |    48.90ms |    48.90ms |
|                         |          |       |            |            |            |
|        `host_to_device` |   0.04s  |     3 |    11.12ms |    11.99ms |    12.91ms |
|          `apply_filter` | 166.29s  |     3 | 54399.08ms | 55431.50ms | 56299.33ms |
|        `device_to_host` |   0.07s  |     3 |    21.62ms |    22.15ms |    22.44ms |

