
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.78s  |     1 | 784.68ms | 784.68ms | 784.68ms |
|           `init_filter` | 0.07s  |     1 |  66.99ms |  66.99ms |  66.99ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.07s  |     1 |  72.45ms |  72.45ms |  72.45ms |
|   `apply_filter` (cold) | 0.30s  |     1 | 295.20ms | 295.20ms | 295.20ms |
| `device_to_host` (cold) | 0.07s  |     1 |  72.78ms |  72.78ms |  72.78ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.02s  |    15 |   0.88ms |   1.09ms |   1.71ms |
|          `apply_filter` | 0.03s  |    15 |   1.58ms |   1.67ms |   2.06ms |
|        `device_to_host` | 0.01s  |    15 |   0.28ms |   0.34ms |   0.53ms |

