
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.21s  |     1 | 207.87ms | 207.87ms | 207.87ms |
|           `init_filter` | 0.04s  |     1 |  35.87ms |  35.87ms |  35.87ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.03s  |     1 |  31.31ms |  31.31ms |  31.31ms |
|   `apply_filter` (cold) | 0.30s  |     1 | 303.86ms | 303.86ms | 303.86ms |
| `device_to_host` (cold) | 0.03s  |     1 |  29.74ms |  29.74ms |  29.74ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.01s  |     3 |   2.77ms |   3.19ms |   3.58ms |
|          `apply_filter` | 0.44s  |     3 | 144.45ms | 145.40ms | 146.04ms |
|        `device_to_host` | 0.01s  |     3 |   2.12ms |   2.23ms |   2.28ms |

