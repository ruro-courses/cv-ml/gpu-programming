
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.77s  |     1 | 765.55ms | 765.55ms | 765.55ms |
|           `init_filter` | 0.07s  |     1 |  66.82ms |  66.82ms |  66.82ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.08s  |     1 |  77.85ms |  77.85ms |  77.85ms |
|   `apply_filter` (cold) | 0.30s  |     1 | 298.40ms | 298.40ms | 298.40ms |
| `device_to_host` (cold) | 0.08s  |     1 |  75.22ms |  75.22ms |  75.22ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.12s  |    15 |   6.75ms |   7.70ms |   9.46ms |
|          `apply_filter` | 0.28s  |    15 |  18.60ms |  18.86ms |  19.05ms |
|        `device_to_host` | 0.04s  |    15 |   1.69ms |   2.60ms |  10.75ms |

