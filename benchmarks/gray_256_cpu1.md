
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.21s  |     1 | 211.50ms | 211.50ms | 211.50ms |
|           `init_filter` | 0.00s  |     1 |   0.24ms |   0.24ms |   0.24ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.04s  |     1 |  37.32ms |  37.32ms |  37.32ms |
|   `apply_filter` (cold) | 0.09s  |     1 |  89.75ms |  89.75ms |  89.75ms |
| `device_to_host` (cold) | 0.03s  |     1 |  32.28ms |  32.28ms |  32.28ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.01s  |     3 |   1.73ms |   1.80ms |   1.91ms |
|          `apply_filter` | 0.02s  |     3 |   4.84ms |   5.09ms |   5.24ms |
|        `device_to_host` | 0.00s  |     3 |   1.50ms |   1.63ms |   1.77ms |

