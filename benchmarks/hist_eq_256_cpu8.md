
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.21s  |     1 | 205.78ms | 205.78ms | 205.78ms |
|           `init_filter` | 0.03s  |     1 |  34.50ms |  34.50ms |  34.50ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.04s  |     1 |  35.62ms |  35.62ms |  35.62ms |
|   `apply_filter` (cold) | 0.64s  |     1 | 641.39ms | 641.39ms | 641.39ms |
| `device_to_host` (cold) | 0.03s  |     1 |  28.09ms |  28.09ms |  28.09ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.00s  |     3 |   0.87ms |   1.19ms |   1.82ms |
|          `apply_filter` | 0.02s  |     3 |   5.59ms |   6.21ms |   7.20ms |
|        `device_to_host` | 0.00s  |     3 |   0.50ms |   0.56ms |   0.64ms |

