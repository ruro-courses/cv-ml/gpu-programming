
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.20s  |     1 | 202.42ms | 202.42ms | 202.42ms |
|           `init_filter` | 0.00s  |     1 |   0.16ms |   0.16ms |   0.16ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.47s  |     1 | 467.83ms | 467.83ms | 467.83ms |
|   `apply_filter` (cold) | 0.71s  |     1 | 713.17ms | 713.17ms | 713.17ms |
| `device_to_host` (cold) | 0.35s  |     1 | 354.27ms | 354.27ms | 354.27ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 1.09s  |     3 | 359.81ms | 362.63ms | 364.70ms |
|          `apply_filter` | 1.84s  |     3 | 605.15ms | 613.02ms | 618.44ms |
|        `device_to_host` | 0.98s  |     3 | 324.37ms | 327.19ms | 329.39ms |

