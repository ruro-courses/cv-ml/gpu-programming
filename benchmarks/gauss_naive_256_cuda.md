
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.73s  |     1 | 732.80ms | 732.80ms | 732.80ms |
|           `init_filter` | 0.06s  |     1 |  59.71ms |  59.71ms |  59.71ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.06s  |     1 |  63.39ms |  63.39ms |  63.39ms |
|   `apply_filter` (cold) | 0.25s  |     1 | 252.16ms | 252.16ms | 252.16ms |
| `device_to_host` (cold) | 0.06s  |     1 |  62.86ms |  62.86ms |  62.86ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.01s  |    15 |   0.91ms |   0.93ms |   1.01ms |
|          `apply_filter` | 0.47s  |    15 |  29.91ms |  31.47ms |  33.72ms |
|        `device_to_host` | 0.01s  |    15 |   0.33ms |   0.34ms |   0.38ms |

