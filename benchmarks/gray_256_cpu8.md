
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.20s  |     1 | 204.70ms | 204.70ms | 204.70ms |
|           `init_filter` | 0.00s  |     1 |   0.17ms |   0.17ms |   0.17ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.04s  |     1 |  37.68ms |  37.68ms |  37.68ms |
|   `apply_filter` (cold) | 0.08s  |     1 |  83.48ms |  83.48ms |  83.48ms |
| `device_to_host` (cold) | 0.03s  |     1 |  28.34ms |  28.34ms |  28.34ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.01s  |     3 |   1.68ms |   2.03ms |   2.70ms |
|          `apply_filter` | 0.01s  |     3 |   2.69ms |   2.73ms |   2.81ms |
|        `device_to_host` | 0.00s  |     3 |   1.39ms |   1.56ms |   1.88ms |

