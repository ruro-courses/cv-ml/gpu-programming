
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.21s  |     1 | 207.51ms | 207.51ms | 207.51ms |
|           `init_filter` | 0.03s  |     1 |  29.81ms |  29.81ms |  29.81ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.03s  |     1 |  31.65ms |  31.65ms |  31.65ms |
|   `apply_filter` (cold) | 0.26s  |     1 | 259.97ms | 259.97ms | 259.97ms |
| `device_to_host` (cold) | 0.03s  |     1 |  31.67ms |  31.67ms |  31.67ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.00s  |     3 |   1.38ms |   1.42ms |   1.49ms |
|          `apply_filter` | 0.43s  |     3 | 142.64ms | 143.18ms | 144.01ms |
|        `device_to_host` | 0.00s  |     3 |   0.63ms |   0.78ms |   0.86ms |

