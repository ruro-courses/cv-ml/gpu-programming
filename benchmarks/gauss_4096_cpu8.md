
|          Title          |  Total  | Calls |    Min     |    Avg     |    Max     |
| ----------------------: | ------: | ----: | ---------: | ---------: | ---------: |
|           `init_taichi` |  0.19s  |     1 |   193.58ms |   193.58ms |   193.58ms |
|           `init_filter` |  0.03s  |     1 |    29.90ms |    29.90ms |    29.90ms |
|                         |         |       |            |            |            |
| `host_to_device` (cold) |  0.34s  |     1 |   344.10ms |   344.10ms |   344.10ms |
|   `apply_filter` (cold) | 31.52s  |     1 | 31516.69ms | 31516.69ms | 31516.69ms |
| `device_to_host` (cold) |  0.13s  |     1 |   133.56ms |   133.56ms |   133.56ms |
|                         |         |       |            |            |            |
|        `host_to_device` |  0.52s  |     3 |   170.12ms |   171.94ms |   174.25ms |
|          `apply_filter` | 94.47s  |     3 | 31260.12ms | 31491.56ms | 31760.72ms |
|        `device_to_host` |  0.32s  |     3 |   105.25ms |   105.87ms |   106.92ms |

