
|          Title          |  Total  | Calls |    Min     |    Avg     |    Max     |
| ----------------------: | ------: | ----: | ---------: | ---------: | ---------: |
|           `init_taichi` |  0.20s  |     1 |   202.73ms |   202.73ms |   202.73ms |
|           `init_filter` |  0.03s  |     1 |    29.93ms |    29.93ms |    29.93ms |
|                         |         |       |            |            |            |
| `host_to_device` (cold) |  0.55s  |     1 |   547.08ms |   547.08ms |   547.08ms |
|   `apply_filter` (cold) | 32.10s  |     1 | 32097.40ms | 32097.40ms | 32097.40ms |
| `device_to_host` (cold) |  0.36s  |     1 |   360.77ms |   360.77ms |   360.77ms |
|                         |         |       |            |            |            |
|        `host_to_device` |  1.14s  |     3 |   373.06ms |   380.94ms |   395.31ms |
|          `apply_filter` | 93.41s  |     3 | 30256.79ms | 31136.20ms | 32058.55ms |
|        `device_to_host` |  0.98s  |     3 |   322.98ms |   325.10ms |   327.82ms |

