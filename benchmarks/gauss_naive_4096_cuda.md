
|          Title          |  Total   | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -------: | ----: | --------: | --------: | --------: |
|           `init_taichi` |   0.84s  |     1 |  835.76ms |  835.76ms |  835.76ms |
|           `init_filter` |   0.07s  |     1 |   71.62ms |   71.62ms |   71.62ms |
|                         |          |       |           |           |           |
| `host_to_device` (cold) |   0.17s  |     1 |  174.82ms |  174.82ms |  174.82ms |
|   `apply_filter` (cold) |   7.95s  |     1 | 7946.36ms | 7946.36ms | 7946.36ms |
| `device_to_host` (cold) |   0.12s  |     1 |  123.06ms |  123.06ms |  123.06ms |
|                         |          |       |           |           |           |
|        `host_to_device` |   1.63s  |    15 |  102.85ms |  108.44ms |  119.20ms |
|          `apply_filter` | 116.55s  |    15 | 7708.65ms | 7769.89ms | 7829.95ms |
|        `device_to_host` |   0.75s  |    15 |   47.90ms |   49.69ms |   54.31ms |

