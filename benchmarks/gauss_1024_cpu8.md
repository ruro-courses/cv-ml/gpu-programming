
|          Title          | Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -----: | ----: | --------: | --------: | --------: |
|           `init_taichi` | 0.21s  |     1 |  209.81ms |  209.81ms |  209.81ms |
|           `init_filter` | 0.03s  |     1 |   29.71ms |   29.71ms |   29.71ms |
|                         |        |       |           |           |           |
| `host_to_device` (cold) | 0.06s  |     1 |   57.97ms |   57.97ms |   57.97ms |
|   `apply_filter` (cold) | 2.11s  |     1 | 2105.21ms | 2105.21ms | 2105.21ms |
| `device_to_host` (cold) | 0.03s  |     1 |   34.47ms |   34.47ms |   34.47ms |
|                         |        |       |           |           |           |
|        `host_to_device` | 0.04s  |     3 |   12.66ms |   12.73ms |   12.88ms |
|          `apply_filter` | 6.01s  |     3 | 1994.69ms | 2002.72ms | 2015.91ms |
|        `device_to_host` | 0.02s  |     3 |    5.75ms |    6.30ms |    6.93ms |

