
|          Title          | Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -----: | ----: | --------: | --------: | --------: |
|           `init_taichi` | 0.20s  |     1 |  203.59ms |  203.59ms |  203.59ms |
|           `init_filter` | 0.00s  |     1 |    0.14ms |    0.14ms |    0.14ms |
|                         |        |       |           |           |           |
| `host_to_device` (cold) | 0.46s  |     1 |  464.58ms |  464.58ms |  464.58ms |
|   `apply_filter` (cold) | 1.22s  |     1 | 1221.87ms | 1221.87ms | 1221.87ms |
| `device_to_host` (cold) | 0.35s  |     1 |  352.90ms |  352.90ms |  352.90ms |
|                         |        |       |           |           |           |
|        `host_to_device` | 1.09s  |     3 |  359.83ms |  362.75ms |  366.21ms |
|          `apply_filter` | 3.35s  |     3 | 1111.06ms | 1116.76ms | 1123.42ms |
|        `device_to_host` | 0.98s  |     3 |  326.39ms |  327.72ms |  329.88ms |

