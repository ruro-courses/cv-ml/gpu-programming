
|          Title          | Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -----: | ----: | --------: | --------: | --------: |
|           `init_taichi` | 0.71s  |     1 |  708.91ms |  708.91ms |  708.91ms |
|           `init_filter` | 0.07s  |     1 |   69.08ms |   69.08ms |   69.08ms |
|                         |        |       |           |           |           |
| `host_to_device` (cold) | 0.15s  |     1 |  154.92ms |  154.92ms |  154.92ms |
|   `apply_filter` (cold) | 1.58s  |     1 | 1580.25ms | 1580.25ms | 1580.25ms |
| `device_to_host` (cold) | 0.11s  |     1 |  105.61ms |  105.61ms |  105.61ms |
|                         |        |       |           |           |           |
|        `host_to_device` | 1.33s  |    15 |   87.06ms |   88.70ms |   90.05ms |
|          `apply_filter` | 0.29s  |    15 |   19.44ms |   19.51ms |   19.71ms |
|        `device_to_host` | 0.66s  |    15 |   43.00ms |   44.17ms |   47.07ms |

