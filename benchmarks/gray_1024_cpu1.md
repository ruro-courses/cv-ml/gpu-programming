
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.20s  |     1 | 203.51ms | 203.51ms | 203.51ms |
|           `init_filter` | 0.00s  |     1 |   0.15ms |   0.15ms |   0.15ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.06s  |     1 |  63.30ms |  63.30ms |  63.30ms |
|   `apply_filter` (cold) | 0.15s  |     1 | 154.00ms | 154.00ms | 154.00ms |
| `device_to_host` (cold) | 0.05s  |     1 |  49.05ms |  49.05ms |  49.05ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.07s  |     3 |  22.16ms |  22.88ms |  23.31ms |
|          `apply_filter` | 0.21s  |     3 |  69.88ms |  70.22ms |  70.59ms |
|        `device_to_host` | 0.06s  |     3 |  19.50ms |  19.65ms |  19.75ms |

