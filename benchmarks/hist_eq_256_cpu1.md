
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.20s  |     1 | 202.58ms | 202.58ms | 202.58ms |
|           `init_filter` | 0.03s  |     1 |  33.16ms |  33.16ms |  33.16ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.03s  |     1 |  34.22ms |  34.22ms |  34.22ms |
|   `apply_filter` (cold) | 0.67s  |     1 | 671.83ms | 671.83ms | 671.83ms |
| `device_to_host` (cold) | 0.03s  |     1 |  30.17ms |  30.17ms |  30.17ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.01s  |     3 |   1.83ms |   1.85ms |   1.88ms |
|          `apply_filter` | 0.06s  |     3 |  20.16ms |  20.70ms |  21.20ms |
|        `device_to_host` | 0.00s  |     3 |   1.44ms |   1.47ms |   1.49ms |

