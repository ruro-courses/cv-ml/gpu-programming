
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.76s  |     1 | 759.43ms | 759.43ms | 759.43ms |
|           `init_filter` | 0.00s  |     1 |   2.11ms |   2.11ms |   2.11ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.18s  |     1 | 179.12ms | 179.12ms | 179.12ms |
|   `apply_filter` (cold) | 0.22s  |     1 | 215.97ms | 215.97ms | 215.97ms |
| `device_to_host` (cold) | 0.12s  |     1 | 116.61ms | 116.61ms | 116.61ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 1.54s  |    15 |  96.98ms | 102.37ms | 107.24ms |
|          `apply_filter` | 0.20s  |    15 |  13.29ms |  13.43ms |  13.75ms |
|        `device_to_host` | 0.71s  |    15 |  45.37ms |  47.36ms |  51.27ms |

