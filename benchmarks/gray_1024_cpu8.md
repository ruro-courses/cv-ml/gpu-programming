
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.20s  |     1 | 204.44ms | 204.44ms | 204.44ms |
|           `init_filter` | 0.00s  |     1 |   0.16ms |   0.16ms |   0.16ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.06s  |     1 |  62.54ms |  62.54ms |  62.54ms |
|   `apply_filter` (cold) | 0.12s  |     1 | 120.26ms | 120.26ms | 120.26ms |
| `device_to_host` (cold) | 0.05s  |     1 |  49.24ms |  49.24ms |  49.24ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.07s  |     3 |  24.07ms |  24.55ms |  25.13ms |
|          `apply_filter` | 0.11s  |     3 |  37.93ms |  38.23ms |  38.65ms |
|        `device_to_host` | 0.06s  |     3 |  20.70ms |  20.81ms |  20.91ms |

