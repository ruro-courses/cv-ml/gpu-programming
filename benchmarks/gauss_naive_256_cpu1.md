
|          Title          |  Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | ------: | ----: | --------: | --------: | --------: |
|           `init_taichi` |  0.18s  |     1 |  182.75ms |  182.75ms |  182.75ms |
|           `init_filter` |  0.03s  |     1 |   27.21ms |   27.21ms |   27.21ms |
|                         |         |       |           |           |           |
| `host_to_device` (cold) |  0.03s  |     1 |   29.50ms |   29.50ms |   29.50ms |
|   `apply_filter` (cold) |  3.48s  |     1 | 3477.96ms | 3477.96ms | 3477.96ms |
| `device_to_host` (cold) |  0.04s  |     1 |   36.59ms |   36.59ms |   36.59ms |
|                         |         |       |           |           |           |
|        `host_to_device` |  0.01s  |     3 |    2.49ms |    2.60ms |    2.76ms |
|          `apply_filter` | 10.15s  |     3 | 3380.39ms | 3381.87ms | 3383.31ms |
|        `device_to_host` |  0.01s  |     3 |    1.98ms |    2.03ms |    2.12ms |

