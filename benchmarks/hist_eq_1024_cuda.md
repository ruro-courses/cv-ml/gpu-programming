
|          Title          | Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -----: | ----: | --------: | --------: | --------: |
|           `init_taichi` | 0.81s  |     1 |  808.88ms |  808.88ms |  808.88ms |
|           `init_filter` | 0.08s  |     1 |   79.26ms |   79.26ms |   79.26ms |
|                         |        |       |           |           |           |
| `host_to_device` (cold) | 0.09s  |     1 |   85.32ms |   85.32ms |   85.32ms |
|   `apply_filter` (cold) | 1.71s  |     1 | 1713.84ms | 1713.84ms | 1713.84ms |
| `device_to_host` (cold) | 0.07s  |     1 |   70.43ms |   70.43ms |   70.43ms |
|                         |        |       |           |           |           |
|        `host_to_device` | 0.10s  |    15 |    5.53ms |    6.35ms |    7.74ms |
|          `apply_filter` | 0.03s  |    15 |    1.72ms |    1.77ms |    1.94ms |
|        `device_to_host` | 0.03s  |    15 |    1.44ms |    2.09ms |    3.76ms |

