
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.72s  |     1 | 719.63ms | 719.63ms | 719.63ms |
|           `init_filter` | 0.06s  |     1 |  63.86ms |  63.86ms |  63.86ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.08s  |     1 |  75.73ms |  75.73ms |  75.73ms |
|   `apply_filter` (cold) | 0.70s  |     1 | 701.24ms | 701.24ms | 701.24ms |
| `device_to_host` (cold) | 0.07s  |     1 |  65.17ms |  65.17ms |  65.17ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.11s  |    15 |   6.54ms |   7.07ms |   9.29ms |
|          `apply_filter` | 7.18s  |    15 | 474.88ms | 478.84ms | 484.59ms |
|        `device_to_host` | 0.03s  |    15 |   1.66ms |   1.79ms |   2.62ms |

