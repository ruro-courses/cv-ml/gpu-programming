
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.79s  |     1 | 791.77ms | 791.77ms | 791.77ms |
|           `init_filter` | 0.00s  |     1 |   0.27ms |   0.27ms |   0.27ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.08s  |     1 |  82.69ms |  82.69ms |  82.69ms |
|   `apply_filter` (cold) | 0.20s  |     1 | 199.16ms | 199.16ms | 199.16ms |
| `device_to_host` (cold) | 0.07s  |     1 |  69.29ms |  69.29ms |  69.29ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.10s  |    15 |   5.67ms |   6.60ms |   8.36ms |
|          `apply_filter` | 0.02s  |    15 |   0.99ms |   1.04ms |   1.43ms |
|        `device_to_host` | 0.03s  |    15 |   1.44ms |   1.99ms |   3.26ms |

