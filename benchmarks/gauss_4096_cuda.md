
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.79s  |     1 | 790.57ms | 790.57ms | 790.57ms |
|           `init_filter` | 0.07s  |     1 |  69.70ms |  69.70ms |  69.70ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.18s  |     1 | 181.61ms | 181.61ms | 181.61ms |
|   `apply_filter` (cold) | 0.90s  |     1 | 899.56ms | 899.56ms | 899.56ms |
| `device_to_host` (cold) | 0.12s  |     1 | 121.74ms | 121.74ms | 121.74ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 1.62s  |    15 | 101.25ms | 107.85ms | 125.65ms |
|          `apply_filter` | 4.32s  |    15 | 285.50ms | 287.91ms | 288.75ms |
|        `device_to_host` | 0.72s  |    15 |  47.25ms |  48.33ms |  50.01ms |

