
|          Title          | Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -----: | ----: | --------: | --------: | --------: |
|           `init_taichi` | 0.21s  |     1 |  206.73ms |  206.73ms |  206.73ms |
|           `init_filter` | 0.03s  |     1 |   31.76ms |   31.76ms |   31.76ms |
|                         |        |       |           |           |           |
| `host_to_device` (cold) | 0.07s  |     1 |   67.47ms |   67.47ms |   67.47ms |
|   `apply_filter` (cold) | 2.22s  |     1 | 2218.41ms | 2218.41ms | 2218.41ms |
| `device_to_host` (cold) | 0.05s  |     1 |   54.77ms |   54.77ms |   54.77ms |
|                         |        |       |           |           |           |
|        `host_to_device` | 0.08s  |     3 |   26.34ms |   27.10ms |   28.49ms |
|          `apply_filter` | 6.14s  |     3 | 2027.48ms | 2046.83ms | 2077.91ms |
|        `device_to_host` | 0.07s  |     3 |   21.37ms |   21.77ms |   22.05ms |

