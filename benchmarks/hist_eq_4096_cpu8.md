
|          Title          | Total  | Calls |    Min    |    Avg    |    Max    |
| ----------------------: | -----: | ----: | --------: | --------: | --------: |
|           `init_taichi` | 0.18s  |     1 |  183.66ms |  183.66ms |  183.66ms |
|           `init_filter` | 0.03s  |     1 |   27.83ms |   27.83ms |   27.83ms |
|                         |        |       |           |           |           |
| `host_to_device` (cold) | 0.20s  |     1 |  199.75ms |  199.75ms |  199.75ms |
|   `apply_filter` (cold) | 1.93s  |     1 | 1927.63ms | 1927.63ms | 1927.63ms |
| `device_to_host` (cold) | 0.13s  |     1 |  127.82ms |  127.82ms |  127.82ms |
|                         |        |       |           |           |           |
|        `host_to_device` | 0.46s  |     3 |  141.53ms |  154.24ms |  175.94ms |
|          `apply_filter` | 4.14s  |     3 | 1364.04ms | 1378.35ms | 1401.74ms |
|        `device_to_host` | 0.31s  |     3 |  102.08ms |  104.54ms |  106.51ms |

