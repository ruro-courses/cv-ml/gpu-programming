
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.81s  |     1 | 811.27ms | 811.27ms | 811.27ms |
|           `init_filter` | 0.00s  |     1 |   0.20ms |   0.20ms |   0.20ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.08s  |     1 |  75.79ms |  75.79ms |  75.79ms |
|   `apply_filter` (cold) | 0.20s  |     1 | 201.36ms | 201.36ms | 201.36ms |
| `device_to_host` (cold) | 0.07s  |     1 |  69.13ms |  69.13ms |  69.13ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.01s  |    15 |   0.61ms |   0.69ms |   1.24ms |
|          `apply_filter` | 0.00s  |    15 |   0.20ms |   0.21ms |   0.22ms |
|        `device_to_host` | 0.00s  |    15 |   0.23ms |   0.28ms |   0.75ms |

