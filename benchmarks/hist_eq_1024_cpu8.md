
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.19s  |     1 | 187.26ms | 187.26ms | 187.26ms |
|           `init_filter` | 0.03s  |     1 |  29.44ms |  29.44ms |  29.44ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.04s  |     1 |  43.22ms |  43.22ms |  43.22ms |
|   `apply_filter` (cold) | 0.69s  |     1 | 687.27ms | 687.27ms | 687.27ms |
| `device_to_host` (cold) | 0.03s  |     1 |  32.66ms |  32.66ms |  32.66ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.03s  |     3 |  10.08ms |  11.03ms |  12.23ms |
|          `apply_filter` | 0.25s  |     3 |  83.07ms |  83.73ms |  84.38ms |
|        `device_to_host` | 0.02s  |     3 |   6.39ms |   6.55ms |   6.70ms |

