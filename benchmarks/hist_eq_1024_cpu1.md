
|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.21s  |     1 | 207.34ms | 207.34ms | 207.34ms |
|           `init_filter` | 0.04s  |     1 |  35.99ms |  35.99ms |  35.99ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.06s  |     1 |  60.12ms |  60.12ms |  60.12ms |
|   `apply_filter` (cold) | 0.96s  |     1 | 963.22ms | 963.22ms | 963.22ms |
| `device_to_host` (cold) | 0.05s  |     1 |  52.37ms |  52.37ms |  52.37ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.08s  |     3 |  24.77ms |  25.82ms |  27.66ms |
|          `apply_filter` | 0.93s  |     3 | 307.62ms | 310.73ms | 316.17ms |
|        `device_to_host` | 0.06s  |     3 |  19.88ms |  20.09ms |  20.27ms |

