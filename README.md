# Image Filters

Task description: [link](./task.pdf)

## Summray

| Feature                                                                                               | Notes                                           | Points |
|:------------------------------------------------------------------------------------------------------|:------------------------------------------------|:------:|
| Реализация фильтра Гаусса в один проход (не сепарабельный вариант)                                    | `image_filters apply gauss --use-single-kernel` |   5    |
| Реализация сепарабельного фильтра Гаусса (при наличии сравнения по скорости с более простым аналогом) | `image_filters apply gauss` + см. `Benchmarks`  |   5    |
| Серый мир                                                                                             | `image_filters apply gray`                      |   15   |
| Автоконтраст                                                                                          | `image_filters apply hist_eq`                   |   20   |
|                                                                                                       |                                                 |        |
|                                                                                                       | Итого (максимальный возможный балл):            |   45   |

### Installation

I've implemented these filters using the `taichi` framework that was advertised on the first lecture.

To install this project, you will need `Python3.7` or newer.
You could just `pip install --user .` or you can create and install this project into a virtual environment like this:

```python
python -m venv .venv
. .venv/bin/activate
pip install --upgrade pip
pip install .
```

After this you should have the `image_filters` command available in the virtual environment.
If the `image_filters` command is missing, please ensure that you have activated the virtual environment via `. .venv/bin/activate`.
If you used the `pip install --user .` method, make sure that your `PATH` is set up correctly.

## Usage

The `image_filters` command is implemented in `click`, so all the available subcommands and their options are documented in the applications `--help` messages:

```sh
image_filters --help
image_filters apply --help
image_filters bench --help
image_filters apply gauss --help
# ... etc etc etc
```

### Apply

The `apply` action reads images, runs the specified filter on them and saves them to the specified location.

Here are a few example commands and their results:

```sh
image_filters apply gauss   sample_images/sharp.png        sample_images/sharp_blurred.png          --overwrite
image_filters apply gray    sample_images/im_blue.png      sample_images/im_blue_gray.png           --overwrite
image_filters apply hist_eq sample_images/low_contrast.png sample_images/low_contrast_equalized.png --overwrite
```

|               Before                |                     After                     |
|:-----------------------------------:|:---------------------------------------------:|
|                                     |                                               |
|    ![](sample_images/sharp.png)     |     ![](sample_images/sharp_blurred.png)      |
|                                     |                                               |
|   ![](sample_images/im_blue.png)    |      ![](sample_images/im_blue_gray.png)      |
|                                     |                                               |
| ![](sample_images/low_contrast.png) | ![](sample_images/low_contrast_equalized.png) |
|                                     |                                               |

### Benchmarks

The `bench` action runs the specified filter multiple times, measures and displays the execution time of different parts of the execution pipeline.
It also has a lot of useful debugging/profiling options, that will come in handy later. :^)

Note, that unlike `apply`, the `bench` action doesn't load or save images to disk.
Instead, you specify the desired image dimensions `HxW` and `bench` generates a random image of that size for each run of the filter.

Here are a few example commands to get you started:

```sh
image_filters bench gray 1024x1024 --device cuda
image_filters bench gray 1024x1024 --device vulkan
image_filters bench gray 1024x1024 --device cpu --num-threads 1
image_filters bench gray 1024x1024 --device cpu --num-threads $(nproc)
```

By default, `bench` outputs the timing information for the following sections:

|          Title          | Total  | Calls |   Min    |   Avg    |   Max    |
| ----------------------: | -----: | ----: | -------: | -------: | -------: |
|           `init_taichi` | 0.96s  |     1 | 955.09ms | 955.09ms | 955.09ms |
|           `init_filter` | 0.00s  |     1 |   0.24ms |   0.24ms |   0.24ms |
|                         |        |       |          |          |          |
| `host_to_device` (cold) | 0.06s  |     1 |  57.78ms |  57.78ms |  57.78ms |
|   `apply_filter` (cold) | 0.14s  |     1 | 141.45ms | 141.45ms | 141.45ms |
| `device_to_host` (cold) | 0.05s  |     1 |  48.94ms |  48.94ms |  48.94ms |
|                         |        |       |          |          |          |
|        `host_to_device` | 0.06s  |    15 |   4.25ms |   4.29ms |   4.47ms |
|          `apply_filter` | 0.00s  |    15 |   0.31ms |   0.31ms |   0.33ms |
|        `device_to_host` | 0.02s  |    15 |   1.06ms |   1.06ms |   1.09ms |

Here, `init_taichi` and `init_filter` is the time that it took to prepare the `taichi` compilation framework and to create the filter object.
`host_to_device` and `device_to_host` are the times that were spent copying data between the "host" side `numpy` buffer and the "device" side `taichi` buffers.
`apply_filter` is the time that was spent on actual image processing.

You can get more fine-grained timers for individual functions and kernels to appear by increasing the verbosity with the `-v`/`-vv`/... options.

Note, that some entries in the table have "(cold)" in their names.
These entries record only the first execution of each section.
Taichi compiles the kernels when they are first invoked, so those times are significantly slower than the subsequent runs.

Currently, there is no way to compile these kernels in advance, but the `taichi` developers are working on Ahead-Of-Time compilation as well as caching the compiled kernels between runs.
After these features get released, the "(cold)" execution times (and possibly even some of the `init_taichi` time) should be fixed.

In order to evaluate and compare the performance of my implementations, I created the `./make_benchmarks.sh` helper script.
It runs a grid of different benchmarks and saves them to the [`./benchmarks` folder](./benchmarks) that is distributed alongside this readme.
The tables in the [`./benchmarks` folder](./benchmarks) were obtained on my Laptop with a `GTX 1050` (2 GB VRAM) and a `i7-7700HQ` (4 cores, 8 threads).

## Optimizations

### Separable Gaussian Kernel

As described in the task decription, I've implemented the "naive" single-kernel Gaussian Blur as well as a separable 1xK + Kx1 version.
The separable version is the default and the signle-kernel version can be enabled with the `--use-single-kernel` option.
I measured a 14x improvement when comparing
```python
image_filters bench gauss 4096 --use-single-kernel
```
and
```python
image_filters bench gauss 4096
```
with the default 10.0 value for sigma.

The gap in performance only grows as you increase the kernel size due to the quadratic vs linear complexity.
For example, adding `--sigma 16` makes the performance difference grow to 26x.

### Efficient Parallel Reductions

Full disclosure: This optimizations wasn't programmed by me, but is automatically available via the taichi runtime.
Still, debugging this optimization and getting it to actually work was a whole adventure, and so I feel that I do deserve some credit for it. :^)

Instead of the suggested [Optimizing Parallel Reduction in CUDA][cuda-reduction-1] reduction scheme, my code uses warp-level intrinsics as described in this blogpost [Faster Parallel Reductions on Kepler][cuda-reduction-2].

This optimization is used in the `gray` and `hist_eq` filters.
In `gray` it is used to aggregate the `mean` value across the image.
In `hist_eq` it is used in the `get_cdf_map` kernel to combine the thread-local copies of the histogram.

After some wrangling with the taichi compiler, I was able to make this optimization work.
I have verified that this optimization is enabled (and is working correctly) in 2 ways:

1) You can add the `--dump-ir` option to the `bench` command to export the generated LLVM and NVPTX intermediate representations.
   There, you can observe that the `@llvm.nvvm.shfl.sync.down.i32`/`@llvm.nvvm.shfl.sync.down.f32` and `atomicrmw add` operations are generated.
   I've also verified that the surrounding assembly code implements the `deviceReduceWarpAtomicKernel` function from "Faster Parallel Reductions on Kepler" blogpost.
   And to seal the deal, here is the [corresponding source code in the taichi backend][taichi-warp-reduce].

2) You can add the `--disable-tls` option to the `bench` command to will disable thread local storage, which is required for this optimization.
   After that, you will be able to observe, that the intrinsics are no longer present in the intermediate representations and the performance of the relevant kernels drops significantly.

I measured a 28x improvement when comparing
```python
image_filters bench gray 4096 --disable-tls
```
and
```python
image_filters bench gray 4096
```

### Branch-less Histogram Computation and Lookup Table Querying

This optimization is used in the `hist_eq` filter.
The main idea was briefly discussed in a recent lecture.

The main goal of this optimization is to avoid

1) Indirect memory addressing
2) Branching execution paths

During the histogram accumulation
```python
# This is bad due to indirect memory addressing
histogram[value] += 1
# This is also bad due to branching and thread divergence
if value == 1: histogram[1] += 1
if value == 2: histogram[2] += 1
...
```

And during the CDF mapping through the lookup table
```python
# This is bad due to indirect memory addressing
pixel = lut[value]
# This is also bad due to branching and thread divergence
if value == 1: pixel = lut[1]
if value == 2: pixel = lut[2]
```

Instead, we do this:
```python
histogram[1] += (value == 1)
histogram[2] += (value == 2)
...
```
and this
```python
pixel = select((value == 1), histogram[1], pixel)
pixel = select((value == 2), histogram[2], pixel)
...
```

The select operator chooses the value of either its second or third argument based on the condition in the first argument.
Both `+= (cond)` and `select` operations above actually compile to the same `selp` instruction (and either an `add` or an `ld`).
[The `selp` instruction][ptx-isa-selp] performs a conditional move operation, which allows assigning different values based on a condition **without creating thread divergence**!

Finally, we add some meta-programming magic to avoid manually spelling every line
```python
for i in ti.static(range(N)):
    histogram[i] += (value == idx)
```
and
```python
pixel = 0
for i in ti.static(range(N)):
    pixel = ti.select((value == idx), histogram[idx], pixel)
```
The `ti.static` loops are guaranteed to unroll at compile-time, so the above code is **exactly** equivalent to manually writing out `N` individual lines.

As a final cherry on top, we replace the `+=` with `ti.atomic_add` which enables the Efficient Parallel Reduction optimization, that was described in the previous section.
I measured about a 3x performance increase after implementing this optimization, not counting the performance increase due to Efficient Parallel Reduction.

## Bonus

["taichi is a stable framework with no issues or bugs".][gh-taichi-ruro]


[cuda-reduction-1]: https://developer.download.nvidia.com/assets/cuda/files/reduction.pdf
[cuda-reduction-2]: https://developer.nvidia.com/blog/faster-parallel-reductions-kepler/
[taichi-warp-reduce]: https://github.com/taichi-dev/taichi/blob/22a099dbdb76a2fdd341f7c69c1e34bb77c1690e/taichi/runtime/llvm/runtime.cpp#L1123-L1149
[ptx-isa-selp]: https://docs.nvidia.com/cuda/parallel-thread-execution/index.html#comparison-and-selection-instructions-selp
[gh-taichi-ruro]: https://github.com/taichi-dev/taichi/issues?q=author:RuRo
