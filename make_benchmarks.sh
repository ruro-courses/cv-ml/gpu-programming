#!/bin/sh

. .venv/bin/activate
rm -rf benchmarks
mkdir benchmarks

NPROCS=$(nproc 2>/dev/null)
NPROCS=${NPROCS:-8} # use 8 threads if we couldn't query the hardware

BENCH_CMD="image_filters bench"

run() {
    printf "filter=%-5s size=%-4s device=%-6s\n" "${FILTER_NAME}" "${SIZE}" "${DEVICE_NAME}"
    ${BENCH_CMD} "${@}" > "benchmarks/${FILTER_NAME}_${SIZE}_${DEVICE_NAME}.md"
}

per_device() {
    for device in cuda;
    do
        DEVICE_NAME="${device}" run "${@}" --device "${device}"
    done

    for threads in 1 "${NPROCS}";
    do
        # Only do 4 runs on the CPU because it's so slow
        DEVICE_NAME="cpu${threads}" run "${@}" 4 --device cpu --num-threads "${threads}"
    done
}

per_size() {
    for size in 256 1024 4096;
    do
        SIZE="${size}" per_device "${@}" "${size}"
        printf "\n"
    done
}

per_filter() {
    for filter in gray gauss hist_eq;
    do
        FILTER_NAME="${filter}" per_size "${filter}" "${@}"
    done

    FILTER_NAME="gauss_naive" per_size gauss --use-single-kernel "${@}"
}

per_filter
