#!/bin/sh

if [ "$#" -ne 2 ]; then
    echo "Usage ${0} filter_name path_to_input.png"
    exit 1
fi

. .venv/bin/activate

image_filters apply ${1} ${2} --overwrite out_gpu.png --device cuda
image_filters apply ${1} ${2} --overwrite out_cpu.png --device cpu
